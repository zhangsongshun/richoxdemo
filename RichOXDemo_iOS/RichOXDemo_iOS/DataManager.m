//
//  DataManager.m
//  RichOXDemo_iOS
//
//  Created by richox on 2020/7/29.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import "DataManager.h"

#define ROXDEMO_APPID @"ROXDemo_appid_key"

@implementation DataManager

+ (instancetype)getManager {
    
    static dispatch_once_t onceToken;
    static DataManager * manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [DataManager new];
        [manager initWithLocalData];
    });
    
    return manager;
}

+ (void)saveDataToUserDefault:(NSString *)value key:(NSString *)key {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue: value forKey:key];
    [userDefaults synchronize];
}

+ (NSString *)getDataFromUserDefault: (NSString *)key {
    return  [[NSUserDefaults standardUserDefaults] stringForKey:key];
}

- (void)initWithLocalData {
    self.appId = @"";
    
    if ([DataManager getDataFromUserDefault:ROXDEMO_APPID]) {
        self.appId = [DataManager getDataFromUserDefault:ROXDEMO_APPID];
    }
}

+ (BOOL)saveDataToLocal {
    if ([DataManager getManager].appId != nil && ![[DataManager getManager].appId isEqualToString:@""]) {
        [self saveDataToUserDefault:[DataManager getManager].appId key:ROXDEMO_APPID];

        
        return YES;
    }
    
    return NO;
}

+ (void)clearLocalData {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults removeObjectForKey:ROXDEMO_APPID];
    
    [DataManager getManager].appId = nil;
}

@end
