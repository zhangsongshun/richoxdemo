#import "AppTaskViewController.h"
#import "DataManager.h"
#import <Masonry/Masonry.h>
#import "macro.h"
@import RichOXBase;

@interface AppTaskViewController () <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *missionIdLab;
@property (nonatomic, strong) UITextField *multipleText;
@property (nonatomic, strong) UITextField *bonusText;
@property (nonatomic, strong) UITextField *costText;
@property (nonatomic, strong) UITextField *countText;

@property (nonatomic, strong) NSLock *lock;

@end

@implementation AppTaskViewController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.lock = [[NSLock alloc] init];
    
    UILabel *missionId = [[UILabel alloc] init];
    [self.view addSubview:missionId];
    missionId.text = @"Mission Id:";
    missionId.textColor = [UIColor blueColor];
    missionId.textAlignment = NSTextAlignmentLeft;
    
    [missionId mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(kNavigationBarHeight+10);
        make.left.equalTo(self.view).offset(20);
        make.width.equalTo(@(100));
        make.height.equalTo(@(20));
    }];
    
    
    UITextField *missionIdLab = [[UITextField alloc]init];
    missionIdLab.placeholder = @"mission-id";
    [self.view addSubview:missionIdLab];
    missionIdLab.borderStyle = UITextBorderStyleRoundedRect;
    
    [missionIdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(missionId).offset(-10);
        make.right.equalTo(self.view).offset(-20);
        make.left.equalTo(missionId.mas_right);
        make.height.equalTo(@(40));
    }];
    self.missionIdLab = missionIdLab;
    
    UILabel *mutipleLab = [[UILabel alloc] init];
    [self.view addSubview:mutipleLab];
    mutipleLab.text = @"Multiple:";
    mutipleLab.textColor = [UIColor blueColor];
    mutipleLab.textAlignment = NSTextAlignmentLeft;
    
    [mutipleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(missionIdLab.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(20);
        make.width.equalTo(@(100));
        make.height.equalTo(@(20));
    }];
    
    
    _multipleText = [[UITextField alloc]init];
    _multipleText.text = @"1";
    [self.view addSubview:_multipleText];
    _multipleText.delegate = self;
    _multipleText.borderStyle = UITextBorderStyleRoundedRect;
    
    [_multipleText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(mutipleLab).offset(-10);
        make.right.equalTo(self.view).offset(-20);
        make.left.equalTo(mutipleLab.mas_right);
        make.height.equalTo(@(40));
    }];
    
    UILabel *bonusLab = [[UILabel alloc] init];
    [self.view addSubview:bonusLab];
    bonusLab.text = @"Bonus:";
    bonusLab.textColor = [UIColor blueColor];
    bonusLab.textAlignment = NSTextAlignmentLeft;
    
    [bonusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_multipleText.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(20);
        make.width.equalTo(@(100));
        make.height.equalTo(@(20));
    }];
    
    
    _bonusText = [[UITextField alloc]init];
    _bonusText.text = @"0";
    [self.view addSubview:_bonusText];
    _bonusText.delegate = self;
    _bonusText.borderStyle = UITextBorderStyleRoundedRect;
    
    [_bonusText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bonusLab).offset(-10);
        make.right.equalTo(self.view).offset(-20);
        make.left.equalTo(bonusLab.mas_right);
        make.height.equalTo(@(40));
    }];
    
    UILabel *costLab = [[UILabel alloc] init];
    [self.view addSubview:costLab];
    costLab.text = @"Cost:";
    costLab.textColor = [UIColor blueColor];
    costLab.textAlignment = NSTextAlignmentLeft;
    
    [costLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bonusText.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(20);
        make.width.equalTo(@(100));
        make.height.equalTo(@(20));
    }];
    
    
    _costText = [[UITextField alloc]init];
    _costText.text = @"0";
    [self.view addSubview:_costText];
    _countText.delegate = self;
    _costText.borderStyle = UITextBorderStyleRoundedRect;
    
    [_costText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(costLab).offset(-10);
        make.right.equalTo(self.view).offset(-20);
        make.left.equalTo(costLab.mas_right);
        make.height.equalTo(@(40));
    }];
    
    
    UILabel *countLab = [[UILabel alloc] init];
    [self.view addSubview:countLab];
    countLab.text = @"Count:";
    countLab.textColor = [UIColor blueColor];
    countLab.textAlignment = NSTextAlignmentLeft;
    
    [countLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_costText.mas_bottom).offset(50);
        make.left.equalTo(self.view).offset(50);
        make.width.equalTo(@(100));
        make.height.equalTo(@(20));
    }];
    
    
    _countText = [[UITextField alloc]init];
    _countText.text = @"1";
    [self.view addSubview:_countText];
    _countText.borderStyle = UITextBorderStyleRoundedRect;
    _countText.delegate = self;
    [_countText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(countLab).offset(-10);
        make.right.equalTo(self.view).offset(-50);
        make.left.equalTo(countLab.mas_right);
        make.height.equalTo(@(40));
    }];
    
    
    UIButton *doTaskBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:doTaskBtn];
    doTaskBtn.layer.cornerRadius = 5;
    doTaskBtn.backgroundColor  = [UIColor colorWithRed:28.0/255.0 green:147.0/255.0 blue:243.0/255.0 alpha:1.0];
    [doTaskBtn setTitle:@"Do Task" forState:UIControlStateNormal];
    [doTaskBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [doTaskBtn addTarget:self action:@selector(doTask) forControlEvents:UIControlEventTouchUpInside];
    
    [doTaskBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_countText.mas_bottom).offset(20);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(180));
        make.height.equalTo(@(40));
    }];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:backBtn];
    backBtn.layer.cornerRadius = 5;
    backBtn.backgroundColor  = [UIColor lightGrayColor];
    [backBtn setTitle:@"Back" forState:UIControlStateNormal];
    [backBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(doTaskBtn.mas_bottom).offset(20);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(180));
        make.height.equalTo(@(40));
    }];

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.multipleText) {
        return [self validateNumber:string];
    } else if (textField == self.countText) {
        return range.location<=1 && [self validateNumber:string];
    } else {
        if ([textField.text rangeOfString:@"."].location==NSNotFound && range.location != 0){
            return [self validateFloat:string];
        } else {
            return [self validateNumber:string];
        }
    }
}

- (BOOL)validateNumber:(NSString*)number{
   BOOL res = YES;
   NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
   int i = 0;
   while (i < number.length) {
       NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
       NSRange range = [string rangeOfCharacterFromSet:tmpSet];
       if (range.length == 0) {
           res = NO;
           break;
       }
       i++;
   }
   return res;
}

- (BOOL)validateFloat: (NSString *)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}

- (void)setupComfirmController: (NSString *)content{
     UIAlertController *vc = [UIAlertController alertControllerWithTitle:@"温馨提示" message:content preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionConfim = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:nil];
    [vc addAction:actionConfim];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)back {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)doTask {
    if (self.missionIdLab.text == nil || [self.missionIdLab.text isEqualToString:@""]) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [self setupComfirmController:@"请输入mission-id"];
        });
        
        return;
    }
    int count = [self.countText.text intValue];
    __block int success = 0;
    __block int fail = 0;
    __block int total = count;
    
    while (count > 0) {
        [RichOXMission submitMissionInfo:self.missionIdLab.text multiple:self.multipleText.text bouns:[self.bonusText.text floatValue] cost:[self.costText.text floatValue] success:^(RichOXMissionSubmitResult * _Nonnull result) {
            [self.lock lock];
                success++;
                if (success+fail == total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                    [self setupComfirmController:[NSString stringWithFormat:@"执行任务成功%d, 失败%d",success, fail]];
                    });
                }
            [self.lock unlock];
        } failure:^(NSError * _Nonnull error) {
            [self.lock lock];
                fail++;
                if (success+fail == total) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                    [self setupComfirmController:[NSString stringWithFormat:@"执行任务成功%d, 失败%d",success, fail]];
                    });
                }
            [self.lock unlock];
        }];
        
        count--;
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
