//
//  TestTabPageViewController.h
//  RichOXDemo
//
//  Created by richox on 2020/6/30.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import <UIKit/UIKit.h>
@import RichOX;

NS_ASSUME_NONNULL_BEGIN

@interface TestTabPageViewController : UIViewController

- (void)loadTabView: (NSString *)sceceEntryId;
- (void)setContentStr: (NSString *)content;

@end

NS_ASSUME_NONNULL_END
