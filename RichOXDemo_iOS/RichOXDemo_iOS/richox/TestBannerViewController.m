//
//  TestBannerViewController.m
//  RichOXDemo
//
//  Created by richox on 2020/6/30.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import "TestBannerViewController.h"
@import RichOX;
#import "UIView+Toast.h"
#import <Masonry/Masonry.h>
#import "macro.h"

@interface TestBannerViewController () <ROXBannerSceneDelegate>

@property (nonatomic, strong) ROXBannerView *LottieBannerView;
@property (nonatomic, strong) UIView *LottieView;

@property (nonatomic, strong) ROXBannerView *HtmlBannerView;
@property (nonatomic, strong) UIView *htmlView;

@property (nonatomic, strong) ROXBannerView *ImageBannerView;
@property (nonatomic, strong) UIView *imageView;

@property (nonatomic, strong) UITextField *imageText;

@property (nonatomic, strong) UITextField *lottieText;

@property (nonatomic, strong) UITextField *htmlText;

@property (nonatomic, strong) NSString *lastImageId;
@property (nonatomic, strong) NSString *lastLottieId;
@property (nonatomic, strong) NSString *lastHtmlId;

@end

@implementation TestBannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    UITextField *textField1 = [[UITextField alloc]init];
    [self.view addSubview:textField1];
    textField1.borderStyle = UITextBorderStyleRoundedRect;

    self.lottieText = textField1;
    
    [textField1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(kNavigationBarHeight+20);
        make.left.equalTo(self.view).offset(20);
        make.height.equalTo(@(40));
    }];
    
    UIButton *loadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:loadBtn];
    [loadBtn setTitle:@"load Lottie Banner" forState:UIControlStateNormal];
    [loadBtn setTitleColor:[UIColor colorWithRed:28.0/255.0 green:147.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [loadBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:216.0/255.0 blue:80.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    [loadBtn setTitleColor:[UIColor lightGrayColor]  forState:UIControlStateDisabled];
    [loadBtn addTarget:self action:@selector(loadLottieBanner) forControlEvents:UIControlEventTouchUpInside];
    
    [loadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textField1);
        make.left.equalTo(textField1.mas_right).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.width.equalTo(@(180));
        make.height.equalTo(@(40));
    }];
    
    
    self.LottieView = [[UIView alloc] init];
    self.LottieView.layer.borderWidth = 2;
    self.LottieView.layer.borderColor = [UIColor redColor].CGColor;
    [self.view addSubview:self.LottieView];
    self.LottieView.hidden = YES;
    
    [self.LottieView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textField1.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.equalTo(@(100));
    }];
    
    UITextField *textField2 = [[UITextField alloc]init];
    [self.view addSubview:textField2];
    textField2.borderStyle = UITextBorderStyleRoundedRect;

    self.htmlText = textField2;
    
    [textField2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.LottieView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(20);
        make.height.equalTo(@(40));
    }];
    
    
    UIButton *loadHtmlBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:loadHtmlBtn];
    [loadHtmlBtn setTitle:@"load HTML Banner" forState:UIControlStateNormal];
    [loadHtmlBtn setTitleColor:[UIColor colorWithRed:28.0/255.0 green:147.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [loadHtmlBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:216.0/255.0 blue:80.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    [loadHtmlBtn setTitleColor:[UIColor lightGrayColor]  forState:UIControlStateDisabled];
    [loadHtmlBtn addTarget:self action:@selector(loadHtmlBanner) forControlEvents:UIControlEventTouchUpInside];
    
    [loadHtmlBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textField2);
        make.left.equalTo(textField2.mas_right).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.width.equalTo(@(180));
        make.height.equalTo(@(40));
    }];
    
    
    self.htmlView = [[UIView alloc] init];
    self.htmlView.layer.borderWidth = 2;
    self.htmlView.layer.borderColor = [UIColor yellowColor].CGColor;
    [self.view addSubview:self.htmlView];
    self.htmlView.hidden = YES;
    
    [self.htmlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textField2.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.equalTo(@(130));
    }];
    
    UITextField *textField3 = [[UITextField alloc]init];
    [self.view addSubview:textField3];
    textField3.borderStyle = UITextBorderStyleRoundedRect;

    self.imageText = textField3;
    
    [textField3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.htmlView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(20);
        make.height.equalTo(@(40));
    }];
    
    UIButton *loadImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:loadImageBtn];
    [loadImageBtn setTitle:@"load Image Banner" forState:UIControlStateNormal];
    [loadImageBtn setTitleColor:[UIColor colorWithRed:28.0/255.0 green:147.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [loadImageBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:216.0/255.0 blue:80.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    [loadImageBtn setTitleColor:[UIColor lightGrayColor]  forState:UIControlStateDisabled];
    [loadImageBtn addTarget:self action:@selector(loadImageBanner) forControlEvents:UIControlEventTouchUpInside];
    
    [loadImageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textField3);
        make.left.equalTo(textField3.mas_right).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.width.equalTo(@(180));
        make.height.equalTo(@(40));
    }];
    
    
    self.imageView = [[UIView alloc] init];
    self.imageView.layer.borderWidth = 2;
    self.imageView.layer.borderColor = [UIColor greenColor].CGColor;
    self.imageView.hidden = YES;
    [self.view addSubview:self.imageView];
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textField3.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.bottom.equalTo(self.view).offset(-kBottomTabBarHeight-20);
    }];
}

- (void)loadLottieBanner {
    if(![ROXManager richOXInited]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view makeToast:@"sdk not inited please click \"start\" button to start it!" duration:3.0 position:CSToastPositionCenter];
        });
        return;
    }
    if (self.LottieBannerView == nil  || ![self.lastLottieId isEqualToString:self.lottieText.text]) {
        if (self.lottieText.text != nil && ![self.lottieText.text isEqualToString:@""]) {
            self.LottieBannerView = [[ROXBannerView alloc] initWithSceneEntryId:self.lottieText.text containerView: self.LottieView viewController: self delegate:self];
            self.lastLottieId = self.lottieText.text;
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:@"please input scence entry id " duration:3.0 position:CSToastPositionCenter];
            });
        }
    }
    
    if (self.LottieBannerView != nil) {
        [self.LottieBannerView load];
    }
}

- (void)loadHtmlBanner {
    if(![ROXManager richOXInited]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view makeToast:@"sdk not inited please click \"start\" button to start it!" duration:3.0 position:CSToastPositionCenter];
        });
        return;
    }
    
    
    if (self.HtmlBannerView == nil  || ![self.lastHtmlId isEqualToString:self.htmlText.text]) {
        if (self.htmlText.text != nil && ![self.htmlText.text isEqualToString:@""]) {
            self.HtmlBannerView = [[ROXBannerView alloc] initWithSceneEntryId:self.htmlText.text containerView: self.htmlView viewController: self delegate:self];
            self.lastHtmlId = self.htmlText.text;
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:@"please input scence entry id " duration:3.0 position:CSToastPositionCenter];
            });
        }
    }
    if (self.HtmlBannerView != nil) {
        [self.HtmlBannerView load];
    }
}

- (void)loadImageBanner {
    if(![ROXManager richOXInited]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view makeToast:@"sdk not inited please click \"start\" button to start it!" duration:3.0 position:CSToastPositionCenter];
        });
        return;
    }
    if (self.ImageBannerView == nil ||![self.lastImageId isEqualToString:self.imageText.text]) {
        if (self.imageText.text != nil && ![self.imageText.text isEqualToString:@""]) {
            self.ImageBannerView = [[ROXBannerView alloc] initWithSceneEntryId:self.imageText.text containerView: self.imageView viewController: self delegate:self];
            self.lastImageId = self.imageText.text;
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:@"please input scence entry id " duration:3.0 position:CSToastPositionCenter];
            });
        }
    }
    if (self.ImageBannerView != nil) {
        [self.ImageBannerView load];
    }
}


- (void)sceneDidLoaded:(NSObject *)scene {
    if (scene == self.LottieBannerView) {
        if ([self.LottieBannerView sceneRenderReady]) {
            self.LottieView.hidden = NO;
        }
    } else if (scene == self.HtmlBannerView){
        if ([self.HtmlBannerView sceneRenderReady]) {
            self.htmlView.hidden = NO;
        }
    } else if (scene == self.ImageBannerView){
        if ([self.ImageBannerView sceneRenderReady]) {
            self.imageView.hidden = NO;
        }
    }
}
- (void)scene:(NSObject *)scene didLoadedFailWithError:(ROXError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
    if (scene == self.LottieBannerView) {
        [self.view makeToast:@"load lottie banner failed" duration:3.0 position:CSToastPositionCenter];
    } else if (scene == self.HtmlBannerView){
        [self.view makeToast:@"load html banner failed" duration:3.0 position:CSToastPositionCenter];
    } else if (scene == self.ImageBannerView){
        [self.view makeToast:@"load image banner failed" duration:3.0 position:CSToastPositionCenter];
    }
    });
}


- (void)sceneRendered:(NSObject *)scene {
    
}

- (void)scene:(NSObject *)scene didRenderFailWithError:(ROXError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
    if (scene == self.LottieBannerView) {
        [self.view makeToast:@"render lottie banner failed" duration:3.0 position:CSToastPositionCenter];
    } else if (scene == self.HtmlBannerView){
        [self.view makeToast:@"render html banner failed" duration:3.0 position:CSToastPositionCenter];
    } else if (scene == self.ImageBannerView){
        [self.view makeToast:@"render image banner failed" duration:3.0 position:CSToastPositionCenter];
    }
    });
}


- (void)sceneWillPresentScreen:(NSObject *)scene {
    
}
- (void)sceneDidPresentScreen:(NSObject *)scene {
    
}
- (void)sceneWillDismissScreen:(NSObject *)scene {
    
}
- (void)sceneDidDismissScreen:(NSObject *)scene {
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
