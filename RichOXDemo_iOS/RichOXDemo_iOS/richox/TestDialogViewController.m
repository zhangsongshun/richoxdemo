//
//  TestDialogViewController.m
//  RichOXDemo
//
//  Created by richox on 2020/6/30.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import "TestDialogViewController.h"
@import RichOX;
#import "UIView+Toast.h"
#import <Masonry/Masonry.h>
#import "macro.h"

@interface TestDialogViewController () <ROXDialogSceneDelegate>

@property (nonatomic, strong) ROXDialog *dialog;

@property (nonatomic, strong) UIButton *showBtn;
@property (nonatomic, strong) UIButton *destoryBtn;

@property (nonatomic, strong) UITextField *entryIdText;

@property (nonatomic, strong) NSString *lastId;

@end

@implementation TestDialogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *dialoglab = [[UILabel alloc] init];
    dialoglab.text = @"dialog entry id: ";
    [self.view addSubview:dialoglab];
    
    [dialoglab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(kNavigationBarHeight+20);
        make.left.equalTo(self.view).offset(20);
        make.width.equalTo(@(140));
        make.height.equalTo(@(30));
    }];
    
    UITextField *textField = [[UITextField alloc]init];
    [self.view addSubview:textField];
    textField.borderStyle = UITextBorderStyleRoundedRect;

    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(dialoglab);
        make.left.equalTo(dialoglab.mas_right).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.height.equalTo(@(30));
    }];
    
    self.entryIdText = textField;
    
    
    UIButton *loadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:loadBtn];
    [loadBtn setTitle:@"Load Dialog" forState:UIControlStateNormal];
    [loadBtn setTitleColor:[UIColor colorWithRed:28.0/255.0 green:147.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [loadBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:216.0/255.0 blue:80.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    [loadBtn setTitleColor:[UIColor lightGrayColor]  forState:UIControlStateDisabled];
    [loadBtn addTarget:self action:@selector(loadDialog) forControlEvents:UIControlEventTouchUpInside];
    
    [loadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textField.mas_bottom).offset(50);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(200));
        make.height.equalTo(@(40));
    }];
    
    
    UIButton *showBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:showBtn];
    [showBtn setTitle:@"Show Dialog" forState:UIControlStateNormal];
    [showBtn setTitleColor:[UIColor colorWithRed:28.0/255.0 green:147.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [showBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:216.0/255.0 blue:80.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    [showBtn setTitleColor:[UIColor lightGrayColor]  forState:UIControlStateDisabled];
    [showBtn addTarget:self action:@selector(showDialog) forControlEvents:UIControlEventTouchUpInside];
    showBtn.enabled = NO;
    self.showBtn = showBtn;
    
    [showBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
        make.width.equalTo(@(200));
        make.height.equalTo(@(40));
    }];
    
    UIButton *destoryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:destoryBtn];
    [destoryBtn setTitle:@"Destory Dialog" forState:UIControlStateNormal];
    [destoryBtn setTitleColor:[UIColor colorWithRed:28.0/255.0 green:147.0/255.0 blue:243.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [destoryBtn setTitleColor:[UIColor colorWithRed:135.0/255.0 green:216.0/255.0 blue:80.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    [destoryBtn setTitleColor:[UIColor lightGrayColor]  forState:UIControlStateDisabled];
    [destoryBtn addTarget:self action:@selector(destoryDialog) forControlEvents:UIControlEventTouchUpInside];
    destoryBtn.enabled = NO;
    self.destoryBtn = destoryBtn;
    
    [destoryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(200));
        make.height.equalTo(@(40));
        make.bottom.equalTo(self.view).offset(-kBottomTabBarHeight-100);
    }];
    
}

- (void)loadDialog {
    if(![ROXManager richOXInited]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view makeToast:@"sdk not inited please click \"start\" button to start it!" duration:3.0 position:CSToastPositionCenter];
        });
        return;
    }
    if (self.dialog == nil  || ![self.lastId isEqualToString:self.entryIdText.text]) {
        if (self.entryIdText.text != nil && ![self.entryIdText.text isEqualToString:@""]) {
            self.dialog = [[ROXDialog alloc] initWithSceneEntryId:self.entryIdText.text delegate:self];
            self.lastId = self.entryIdText.text;
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view makeToast:@"please input scence entry id " duration:3.0 position:CSToastPositionCenter];
            });
        }
    }
    if (self.dialog != nil) {
        [self.dialog load];
    }
}

- (void)showDialog {
    if ([self.dialog sceneRenderReady]) {
        [self.dialog showFromViewController:self];
    }
}

- (void)destoryDialog {
    self.dialog = nil;
    self.showBtn.enabled = NO;
    self.destoryBtn.enabled = NO;
}


- (void)sceneDidLoaded:(NSObject *)scene {
    if (scene == self.dialog) {
        self.showBtn.enabled = YES;
        self.destoryBtn.enabled = YES;
    }
}
- (void)scene:(NSObject *)scene didLoadedFailWithError:(ROXError *)error {
    if (scene == self.dialog) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [self.view makeToast:@"load dialog failed" duration:3.0 position:CSToastPositionCenter];
        });
    }
}

- (void)sceneRendered:(NSObject *)scene {
    
}

- (void)scene:(NSObject *)scene didRenderFailWithError:(ROXError *)error {
    if (scene == self.dialog) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [self.view makeToast:@"render dialog failed" duration:3.0 position:CSToastPositionCenter];
        });
    }
}


- (void)sceneWillPresentScreen:(NSObject *)scene {
    
}
- (void)sceneDidPresentScreen:(NSObject *)scene {
    
}
- (void)sceneWillDismissScreen:(NSObject *)scene {
    
}
- (void)sceneDidDismissScreen:(NSObject *)scene {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
