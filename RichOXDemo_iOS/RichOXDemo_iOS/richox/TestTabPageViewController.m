//
//  TestTabPageViewController.m
//  RichOXDemo
//
//  Created by richox on 2020/6/30.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import "TestTabPageViewController.h"

@interface TestTabPageViewController () <ROXTabSceneDelegate>

@property (nonatomic, strong) ROXTabView *tabView;

@property (nonatomic) BOOL firstOpen;

@property (nonatomic, strong) UIView *tabContainerView;

@property (nonatomic, strong) UILabel *labview;

@end

@implementation TestTabPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
}

- (void)createTabContainerView {
    if (self.tabContainerView == nil) {
        CGFloat statusHeight = [UIApplication sharedApplication].statusBarFrame.size.height;
            //设置选中和未选中的图片...
        self.tabContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, statusHeight+self.navigationController.navigationBar.frame.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height-statusHeight-self.navigationController.navigationBar.bounds.size.height-self.navigationController.tabBarController.tabBar.bounds.size.height)];
        [self.view addSubview:self.tabContainerView];
        self.tabContainerView.hidden = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    if (!self.firstOpen) {
        [self.tabView start];
        self.firstOpen = YES;
    } else {
        [self.tabView resume];
    }
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.tabView pause];
}

- (void)setContentStr: (NSString *)content {
    if (self.labview == nil) {
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(20, 200, [UIScreen mainScreen].bounds.size.width - 40, 200)];
        [lab setFont:[UIFont boldSystemFontOfSize:24]];
        lab.textAlignment = NSTextAlignmentCenter;
        lab.numberOfLines = 0;
        [lab setTextColor:[UIColor purpleColor]];
        
        [self.view addSubview:lab];
        self.labview = lab;
    }
    
    [self.labview setText:content];
}

- (void)loadTabView: (NSString *)sceceEntryId {
    [self createTabContainerView];
    self.tabView = [[ROXTabView alloc] initWithSceneEntryId:sceceEntryId containerView:self.tabContainerView viewController:self delegate:self];
    [self.tabView load];
}

- (void)sceneDidLoaded:(NSObject *)scene {
    NSString *title = [self.tabView getTabTitle];
    if (title != nil && ![title isEqualToString:@""]) {
        self.title = [self.tabView getTabTitle];
    }
    
    self.tabContainerView.hidden = NO;
    
}
- (void)scene:(NSObject *)scene didLoadedFailWithError:(ROXError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setContentStr:@"tab scene load failed"];
    });
}

- (void)sceneRendered:(NSObject *)scene {
    
}

- (void)scene:(NSObject *)scene didRenderFailWithError:(ROXError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setContentStr:@"tab scene render failed"];
    });
}


- (void)sceneWillPresentScreen:(NSObject *)scene {
    
}
- (void)sceneDidPresentScreen:(NSObject *)scene {
    
}
- (void)sceneWillDismissScreen:(NSObject *)scene {
    
}
- (void)sceneDidDismissScreen:(NSObject *)scene {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
