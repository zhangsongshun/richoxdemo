//
//  ROXError.h
//  RichOX
//
//  Created by zena.tang on 2020/4/23.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


#define ROX_ERROR_NO_ACTIVITY             1001
#define ROX_ERROR_ENTRANCE_TYPE_ERROR     1002
#define ROX_ERROR_TIME_NOT_IN_PERIOD      1003
#define ROX_ERROR_ACTIVITY_DATA_ERROR     1004
#define ROX_ERROR_BLANK_ACTIVITY          1005

#define ROX_ERROR_RENDER_TIMEOUT          3001


typedef NS_ENUM(NSInteger, ROX_ERROR_CODE) {
    ROX_ERROR_CODE_INVALID_PARAM    = 1000, // 非法参数
    ROX_ERROR_CODE_INTERNAL         = 2000, // 内部错误
    ROX_ERROR_CODE_RENDER           = 3000, // 渲染错误
    ROX_ERROR_CODE_NETWORK          = 4000, // 网络错误
};


@interface ROXError : NSObject

@property (readonly, copy) NSString   *message;
@property (readonly)       NSInteger  code;
@property (readonly)       ROX_ERROR_CODE  errorCode;

+ (instancetype)renderError: (NSInteger)code message:(NSString *)message;
+ (instancetype)invalidParamError: (NSInteger)code message:(NSString *)message;
+ (instancetype)internalError: (NSInteger)code message:(NSString *)message;
+ (instancetype)networkError;

@end

typedef void (^ROXFailureBlock)(ROXError *error);

NS_ASSUME_NONNULL_END
