//
//  ROXSceneDelegate.h
//  ROX
//
//  Created by zena.tang on 2020/6/22.
//  Copyright © 2020 RichOX. All rights reserved.
//

#ifndef ROXSceneDelegate_h
#define ROXSceneDelegate_h

#import "ROXError.h"

typedef NS_ENUM(NSInteger, ROX_SCENE_ENTRANCE_TYPE) {
    ROX_SCENE_ENTRANCE_TYPE_UNKONWN =0, // 无
    ROX_SCENE_ENTRANCE_TYPE_DIALOG  = 1, // dialog
    ROX_SCENE_ENTRANCE_TYPE_FLOATICON = 2, // 浮标
    ROX_SCENE_ENTRANCE_TYPE_NATIVE = 3, // 原生
    ROX_SCENE_ENTRANCE_TYPE_BANNER = 4, // banner
    ROX_SCENE_ENTRANCE_TYPE_TAB  = 5, // tab
};

@protocol ROXSceneDelegate  <NSObject>

@optional
- (void)sceneDidLoaded:(NSObject *)scene;
- (void)scene:(NSObject *)scene didLoadedFailWithError:(ROXError *)error;

- (void)sceneRendered:(NSObject *)scene;
- (void)scene:(NSObject *)scene didRenderFailWithError:(ROXError *)error;

- (void)sceneWillPresentScreen:(NSObject *)scene;
- (void)sceneDidPresentScreen:(NSObject *)scene;
- (void)sceneWillDismissScreen:(NSObject *)scene;
- (void)sceneDidDismissScreen:(NSObject *)scene;

@end

#endif /* ROXSceneDelegate_h */
