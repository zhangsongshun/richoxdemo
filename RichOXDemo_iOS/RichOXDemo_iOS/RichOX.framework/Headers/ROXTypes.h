//
//  ROXTypes.h
//  RichOX
//
//  Created by RichOX on 2020/7/16.
//  Copyright © 2020 RichOX. All rights reserved.
//

/// Common
typedef const void *ROXTypeRef;


/// ROXManager
typedef const void *ROXTypeManagerClientRef;
typedef const void *ROXTypeManagerRef;

// Event
typedef void (*ROXEventCallback)(ROXTypeManagerClientRef *managerClientRef, 
                                 char *eventName, char *value, char *param);
// Bind WeChat
typedef void (*ROXUnityBindWeChatCallback)(ROXTypeManagerClientRef *managerClientRef);
// Update Gift Info
typedef void (*ROXGiftUpdateCallback)(ROXTypeManagerClientRef *managerClientRef, 
                                           int type, char *title, int amount);


/// FloatScene
// 对 Unity 中 FloatScene 和 FloatSceneClient 的引用。
typedef const void *ROXTypeFloatSceneClientRef;
typedef const void *ROXTypeFloatSceneRef;
// iOS ROXFloatScene 加载后回调 Unity 的接口。
typedef void (*ROXFloatSceneDidReceiveCallback)(ROXTypeFloatSceneClientRef *floatSceneClient);
typedef void (*ROXFloatSceneDidFailToReceiveWithErrorCallback)(ROXTypeFloatSceneClientRef *floatSceneClient, 
                                                               int error, char *message);
typedef void (*ROXFloatSceneWillPresentScreenCallback)(ROXTypeFloatSceneClientRef *floatSceneClient);
typedef void (*ROXFloatSceneDidDismissScreenCallback)(ROXTypeFloatSceneClientRef *floatSceneClient);
typedef void (*ROXFloatSceneWillLeaveApplicationCallback)(ROXTypeFloatSceneClientRef *floatSceneClient);
typedef void (*ROXFloatSceneRenderSuccessCallback)(ROXTypeFloatSceneClientRef *floatSceneClient);
typedef void (*ROXFloatSceneRenderFailedCallback)(ROXTypeFloatSceneClientRef *floatSceneClient, 
                                                  int error, char *message);


/// DialogScene
// 对 Unity 中 DialogScene 和 DialogSceneClient 的引用。
typedef const void *ROXTypeDialogSceneClientRef;
typedef const void *ROXTypeDialogSceneRef;
// iOS ROXDialogScene 加载后回调 Unity 的接口。
typedef void (*ROXDialogSceneDidReceiveCallback)(ROXTypeDialogSceneClientRef *dialogSceneClient);
typedef void (*ROXDialogSceneDidFailToReceiveWithErrorCallback)(ROXTypeDialogSceneClientRef *dialogSceneClient, int error, char *message);
typedef void (*ROXDialogSceneWillPresentScreenCallback)(ROXTypeDialogSceneClientRef *dialogSceneClient);
typedef void (*ROXDialogSceneDidDismissScreenCallback)(ROXTypeDialogSceneClientRef *dialogSceneClient);
typedef void (*ROXDialogSceneWillLeaveApplicationCallback)(ROXTypeDialogSceneClientRef *dialogSceneClient);
typedef void (*ROXDialogSceneRenderSuccessCallback)(ROXTypeDialogSceneClientRef *dialogSceneClient);
typedef void (*ROXDialogSceneRenderFailedCallback)(ROXTypeDialogSceneClientRef *dialogSceneClient, 
                                                  int error, char *message);


/// NativeScene
// 对 Unity 中 NativeScene 和 NativeSceneClient 的引用。
typedef const void *ROXTypeNativeSceneClientRef;
typedef const void *ROXTypeNativeSceneRef;
// iOS ROXNativeScene 加载后回调 Unity 的接口。
typedef void (*ROXNativeSceneDidReceiveCallback)(ROXTypeNativeSceneClientRef *nativeSceneClient);
typedef void (*ROXNativeSceneDidFailToReceiveWithErrorCallback)(ROXTypeNativeSceneClientRef *nativeSceneClient, int error, char *message);
typedef void (*ROXNativeSceneWillPresentScreenCallback)(ROXTypeNativeSceneClientRef *nativeSceneClient);
typedef void (*ROXNativeSceneDidDismissScreenCallback)(ROXTypeNativeSceneClientRef *nativeSceneClient);
typedef void (*ROXNativeSceneWillLeaveApplicationCallback)(ROXTypeNativeSceneClientRef *nativeSceneClient);
typedef void (*ROXNativeSceneRenderSuccessCallback)(ROXTypeNativeSceneClientRef *nativeSceneClient);
typedef void (*ROXNativeSceneRenderFailedCallback)(ROXTypeNativeSceneClientRef *nativeSceneClient, 
                                                  int error, char *message);
typedef void (*ROXNativeSceneUpdateCallback)(ROXTypeNativeSceneClientRef *nativeSceneClient);


/// ROXNativeData
typedef const void *ROXTypeNativeDataRef;
