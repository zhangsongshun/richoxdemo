//
//  ROXDialog.h
//  RichOX
//
//  Created by zena.tang on 2020/6/29.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ROXSceneDelegate.h"
#import "ROXInterActiveDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ROXDialogSceneDelegate  <ROXSceneDelegate>

@end

@interface ROXDialog : NSObject

@property (nonatomic, weak) id<ROXDialogSceneDelegate> delegate;

- (instancetype)initWithSceneEntryId: (NSString *)sceneEntryId delegate: (id<ROXDialogSceneDelegate> _Nullable)delegate;

- (void)load;

- (BOOL)sceneRenderReady;

- (void)showFromViewController: (UIViewController *)viewController;

//interactive活动用
- (void)setIADelegate:(id<ROXInterActiveDelegate>)IADelegate;

- (BOOL) isInterActive: (NSString *)sceneKey;

- (void)loadInterActiveInfo;

- (void)submitInterActiveTask: (NSInteger)taskId;

- (void)fetchInterActiveTaskStatus: (NSInteger)taskId;

@end

NS_ASSUME_NONNULL_END
