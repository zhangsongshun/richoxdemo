//
//  ROXEntranceView.h
//  RichOX
//
//  Created by zena.tang on 2020/6/16.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ROXSceneDelegate.h"
#import "ROXInterActiveDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface ROXEntranceView : UIView

@property (nonatomic, weak) id<ROXSceneDelegate> delegate;

- (instancetype)initWithSceneEntryId: (NSString *)sceneEntryId 
                        entranceType: (ROX_SCENE_ENTRANCE_TYPE)entranceType 
                       containerView: (UIView *) containerView 
                      viewController: (UIViewController *)viewController 
                            delegate: (id<ROXSceneDelegate>)delegate;

- (void)load;

- (BOOL)sceneRenderReady;

//interactive活动用
- (BOOL) isInterActive: (NSString *)sceneKey;
- (void)setIADelegate:(id<ROXInterActiveDelegate>)IADelegate;
- (void)loadInterActiveInfo;
- (void)submitInterActiveTask: (NSInteger)taskId;
- (void)fetchInterActiveTaskStatus: (NSInteger)taskId;

@end

NS_ASSUME_NONNULL_END
