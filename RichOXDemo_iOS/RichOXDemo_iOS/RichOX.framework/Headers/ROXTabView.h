//
//  ROXTabView.h
//  RichOX
//
//  Created by zena.tang on 2020/6/29.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ROXSceneDelegate.h"
#import "ROXInterActiveDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ROXTabSceneDelegate  <ROXSceneDelegate>

@end

@interface ROXTabView : UIView

@property (nonatomic, weak) id<ROXTabSceneDelegate> delegate;

- (instancetype)initWithSceneEntryId: (NSString *)sceneEntryId containerView: (UIView *) containerView viewController: (UIViewController *)viewController delegate: (id<ROXTabSceneDelegate> _Nullable)delegate;

- (void)load;

- (BOOL)sceneRenderReady;

- (NSString * _Nullable)getTabTitle;
- (NSString *_Nullable)getTabIcon;

- (void)start;
- (void)pause;
- (void)resume;


//interactive活动用
- (BOOL) isInterActive: (NSString *)sceneKey;

- (void)setIADelegate:(id<ROXInterActiveDelegate>)IADelegate;

- (void)loadInterActiveInfo;

- (void)submitInterActiveTask: (NSInteger)taskId;

- (void)fetchInterActiveTaskStatus: (NSInteger)taskId;
@end

NS_ASSUME_NONNULL_END
