//
//  ROXNative.h
//  RichOX
//
//  Created by zena.tang on 2020/7/31.
//  Copyright © 2020 richox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ROXSceneDelegate.h"
#import "ROXNativeData.h"
#import "ROXInterActiveDelegate.h"

NS_ASSUME_NONNULL_BEGIN


@protocol ROXNativeSceneDelegate  <ROXSceneDelegate>

@required
- (void)sceneUpdated:(NSObject *)scene;

@end


@interface ROXNative : NSObject

@property (nonatomic, weak) id<ROXNativeSceneDelegate> delegate;

- (instancetype)initWithSceneEntryId: (NSString *)sceneEntryId viewController: (UIViewController *)viewController delegate: (id<ROXNativeSceneDelegate> _Nullable)delegate;

- (void)load;

- (BOOL)sceneRenderReady;

- (ROXNativeData *)nativeEntranceData;

- (void) registerViewForInteraction:(UIView *)rootView clickableViews: (NSArray<UIView *> *) viewList;

- (BOOL) isInterActive: (NSString *)sceneKey;

- (void)setIADelegate:(id<ROXInterActiveDelegate>)IADelegate;

- (void)loadInterActiveInfo;

- (void)submitInterActiveTask: (NSInteger)taskId;

- (void)fetchInterActiveTaskStatus: (NSInteger)taskId;

@end

NS_ASSUME_NONNULL_END
