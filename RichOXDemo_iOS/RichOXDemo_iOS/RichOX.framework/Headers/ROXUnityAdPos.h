//
//  ROXUnityAdPos.h
//  RichOX
//
//  Created by RichOX on 2020/3/29.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ROXUnityAdPosition) {
    ROXUnityAdPos_Custom = -1,      // Custom ad position: (x,y)
    ROXUnityAdPos_Top = 0,          // Top of screen.
    ROXUnityAdPos_Bottom = 1,       // Bottom of screen.
    ROXUnityAdPos_TopLeft = 2,      // Top left of screen.
    ROXUnityAdPos_TopRight = 3,     // Top right of screen.
    ROXUnityAdPos_BottomLeft = 4,   // Bottom left of screen.
    ROXUnityAdPos_BottomRight = 5,  // Bottom right of screen.
    ROXUnityAdPos_Center = 6        // Center of screen.
};

@interface ROXUnityAdPos : NSObject

+ (NSString *)getPositionDesc:(ROXUnityAdPosition)position;

@end

NS_ASSUME_NONNULL_END
