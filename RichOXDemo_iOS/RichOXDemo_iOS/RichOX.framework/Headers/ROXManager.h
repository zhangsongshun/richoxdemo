//
//  ROXManager.h
//  RichOX
//
//  Created by zena.tang on 2020/4/7.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ROXTypes.h"
#import <RichOXBase/RichOXBaseManager.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, ROX_GIFT_TYPE) {
    ROX_GIFT_TYPE_COIN           = 0, // 金币
    ROX_GIFT_TYPE_CHANGE         = 1, // 零钱
    ROX_GIFT_TYPE_POINTS         = 2, // 积分
};

typedef void (^ROXBindWeChatCallBack)(BOOL status, NSString *msg);

typedef void (^ROXBindWeChatBlock)(ROXBindWeChatCallBack block);


typedef void (^ROXGiftUpdateBlock)(ROX_GIFT_TYPE type, NSString *activityTitle, NSInteger amount);

@protocol ROXManagerDelegate  <NSObject>

@optional
- (void)trackCustomEvent: (NSDictionary *)message;

@end

@interface ROXManager : NSObject

@property (nonatomic, readonly) NSString *appId;

@property (nonatomic, weak) id<ROXManagerDelegate> delegate;

+ (instancetype)getManager;

+ (BOOL)richOXInited;

+ (void) initWithAppId: (NSString *)appId userId: (NSString * _Nullable)userId;

/**
@deprecated Please use [RichOXBaseManager setDeviceId]; and  [RichOXBaseManager saveUserId]; replace
*/
+ (void) initWithAppId: (NSString *)appId userId: (NSString * _Nullable)userId  deviceId:(NSString * _Nullable)deviceId  __deprecated;

+ (void)setEventBlock:(RichOXThirdEventBlock)block;
+ (void)setBindWeChatBlock:(ROXBindWeChatBlock)block;
+ (void)setGiftUpdateBlock:(ROXGiftUpdateBlock)block;

+ (int)getSdkVersion;

+ (void)setWDExtendInfo:(NSString *)extendInfo;

/**
@deprecated Please use [RichOXBaseManager setLogEnable:]; replace
*/
+ (void) setLogEnable:(BOOL)enable __deprecated;
/**
@deprecated Please use [RichOXBaseManager isLogEnable];  replace
*/
+ (BOOL) isLogEnable __deprecated;
/**
@deprecated Please use [RichOXBaseManager setTestMode:] replace
*/
+ (void) setTestMode:(BOOL)testMode __deprecated;
/**
@deprecated Please use [RichOXBaseManager setDeviceId]; and  [RichOXBaseManager saveUserId]; replace
*/
+ (BOOL) isTestMode __deprecated;

/**
@deprecated Please use [RichOXBaseManager setAppVerCode:] replace
*/
+ (void) setAppVerCode:(NSInteger) appVerCode __deprecated;

/**
@deprecated Please use [RichOXBaseManager setFissionPlatform];  replace
*/
+ (void) setFissionPlatform:(NSString *)platform __deprecated;

@end

NS_ASSUME_NONNULL_END
