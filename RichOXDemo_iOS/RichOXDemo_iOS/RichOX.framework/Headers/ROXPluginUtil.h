//
//  ROXPluginUtil.h

//
//  用于 Unity 的工具类。

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIKit.h>
#import "ROXTypes.h"
#import "ROXUnityAdPos.h"

@interface ROXPluginUtil : NSObject

/// 将 C 语言 UTF8 编码的 byte Array 转为 NSString；如果 bytes 为 NULL，返回 nil。
+ (NSString *)ROXStringFromUTF8String:(const char *)bytes;

/// 返回 Unity 的 ViewController。
+ (UIViewController *)unityGLViewController;

/// 获取 iOS 当前的 ViewController。
+ (UIViewController *)iOSViewController;

/// 将 view 放在 parentView 的 position 位置。
/// ROXUnityAdPosition.
+ (void)positionView:(UIView *)view inParentView:(UIView *)parentView adPosition:(ROXUnityAdPosition)position;

/// 将 view 放在 parentView 的 (x, y) 位置。
+ (void)positionView:(UIView *)view inParentView:(UIView *)parentView withX:(int)x andY:(int)y;

/// 将 view 放在 parentView 的 position 位置，向左偏移 x，想下偏移 y
+ (void)positionView:(UIView *)view inParentView:(UIView *)parentView adPosition:(ROXUnityAdPosition)position x:(int)x y:(int)y;

@end
