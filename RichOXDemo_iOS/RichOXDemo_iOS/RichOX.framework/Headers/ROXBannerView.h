//
//  ROXBannerView.h
//  RichOX
//
//  Created by zena.tang on 2020/6/28.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ROXEntranceView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ROXBannerSceneDelegate  <ROXSceneDelegate>

@end

@interface ROXBannerView : ROXEntranceView

- (instancetype)initWithSceneEntryId: (NSString *)sceneEntryId containerView: (UIView *) containerView viewController: (UIViewController *)viewController delegate: (id<ROXBannerSceneDelegate> _Nullable)delegate;



@end

NS_ASSUME_NONNULL_END
