//
//  ROXUnityManager.h
//  RichOX
//
//  Created by RichOX on 2020/8/17.
//  Copyright © 2020 richox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ROXTypes.h"

NS_ASSUME_NONNULL_BEGIN

@interface ROXUnityManager : NSObject

+ (ROXUnityManager *)sharedInstance;

@property(nonatomic, assign) ROXTypeManagerClientRef _Nullable* _Nullable managerClient;

@property(nonatomic, assign) ROXEventCallback eventCallback;
@property(nonatomic, assign) ROXUnityBindWeChatCallback unityBindWeChatCallback;
@property(nonatomic, assign) ROXGiftUpdateCallback giftUpdateCallback;

- (void)notifyBindWeChatStatus:(BOOL)status withResult:(NSString *)result;

@end

NS_ASSUME_NONNULL_END
