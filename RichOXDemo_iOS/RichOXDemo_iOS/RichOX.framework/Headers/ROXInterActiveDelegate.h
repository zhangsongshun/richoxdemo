//
//  ROXInterActiveDelegate.h
//  NathAds
//
//  Created by zena.tang on 2020/9/8.
//  Copyright © 2020 richox. All rights reserved.
//

#ifndef ROXInterActiveDelegate_h
#define ROXInterActiveDelegate_h

#import "ROXInterActiveInfo.h"

@protocol ROXInterActiveDelegate  <NSObject>

@optional
- (void)initialized:(NSObject *)scene status:(BOOL)status info:(ROXInterActiveInfo *)info;
- (void)updateFromServer:(NSObject *)scene taskId: (NSInteger)taskId status:(BOOL)status info:(ROXInterActiveInfo *)info;
- (void)updateStatusFormH5:(NSObject *)scene taskId: (NSInteger)taskId status:(BOOL)status progress:(int)progress;

@end


#endif /* ROXInterActiveDelegate_h */
