//
//  ROXFloatView.h
//  RichOX
//
//  Created by zena.tang on 2020/6/29.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ROXEntranceView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ROXFloatSceneDelegate  <ROXSceneDelegate>

@end

@interface ROXFloatView : ROXEntranceView

- (instancetype)initWithSceneEntryId: (NSString *)sceneEntryId containerView: (UIView *) containerView viewController: (UIViewController *)viewController delegate: (id<ROXFloatSceneDelegate> _Nullable)delegate;


@end

NS_ASSUME_NONNULL_END
