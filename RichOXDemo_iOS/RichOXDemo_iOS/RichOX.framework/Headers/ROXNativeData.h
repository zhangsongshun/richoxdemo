//
//  ROXNativeData.h
//  RichOX
//
//  Created by zena.tang on 2020/7/31.
//  Copyright © 2020 richox. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ROXNativeData : NSObject

- (instancetype)initWithSceneObject: (NSObject *)sceneObject;

@property(nonatomic, strong, readonly) NSString *title;
@property(nonatomic, strong, readonly) NSString *desc;
@property(nonatomic, strong, readonly) NSString *iconUrl;
@property(nonatomic, strong, readonly) NSString *callToAction;
@property(nonatomic, strong, readonly) NSString *mediaUrl;

@end

NS_ASSUME_NONNULL_END
