//
//  ROXUnityNative.h
//  RichOX
//
//  Created by RichOX on 2020/8/18.
//  Copyright © 2020 richox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ROXNativeData.h"
#import "ROXTypes.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ROXUnityNative : NSObject

- (instancetype)initWithNativeSceneClient:(ROXTypeNativeSceneClientRef _Nonnull* _Nonnull)client 
                             sceneEntryId:(NSString *)sceneEntryId 
                           viewController:(UIViewController *)viewController;

// 引用 Unity 的 NativeSceneClient
@property(nonatomic, assign) ROXTypeNativeSceneClientRef _Nullable* _Nullable nativeSceneClient;
// 广告加载后，回调到 Unity 的接口
@property(nonatomic, assign) ROXNativeSceneDidReceiveCallback receivedCallback;
@property(nonatomic, assign) ROXNativeSceneDidFailToReceiveWithErrorCallback failedCallback;
@property(nonatomic, assign) ROXNativeSceneWillPresentScreenCallback willPresentCallback;
@property(nonatomic, assign) ROXNativeSceneDidDismissScreenCallback didDismissCallback;
@property(nonatomic, assign) ROXNativeSceneWillLeaveApplicationCallback willLeaveCallback;
@property(nonatomic, assign) ROXNativeSceneRenderSuccessCallback renderSuccessCallback;
@property(nonatomic, assign) ROXNativeSceneRenderFailedCallback renderFailedCallback;
@property(nonatomic, assign) ROXNativeSceneUpdateCallback updateCallback;

- (void)load;

- (BOOL)sceneRenderReady;

- (ROXNativeData *)getNativeInfo;

- (void)reportShown;

- (void)handleClick;

@end

NS_ASSUME_NONNULL_END
