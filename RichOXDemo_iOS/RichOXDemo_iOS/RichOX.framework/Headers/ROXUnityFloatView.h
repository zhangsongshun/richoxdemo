//
//  ROXUnityFloatView.h
//  RichOX
//
//  Created by RichOX on 2020/7/17.
//  Copyright © 2020 RichOX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ROXTypes.h"
#import "ROXUnityAdPos.h"

NS_ASSUME_NONNULL_BEGIN

@interface ROXUnityFloatView : NSObject

- (instancetype)initWithFloatSceneClientRef:(ROXTypeFloatSceneClientRef _Nonnull* _Nonnull)floatSceneClient 
                               sceneEntryId:(NSString *)sceneEntryId 
                             viewController:(UIViewController *)viewController;

// 引用 Unity 的 FloatSceneClient
@property(nonatomic, assign) ROXTypeFloatSceneClientRef _Nullable* _Nullable floatSceneClient;
// 广告加载后，回调到 Unity 的接口
@property(nonatomic, assign) ROXFloatSceneDidReceiveCallback receivedCallback;
@property(nonatomic, assign) ROXFloatSceneDidFailToReceiveWithErrorCallback failedCallback;
@property(nonatomic, assign) ROXFloatSceneWillPresentScreenCallback willPresentCallback;
@property(nonatomic, assign) ROXFloatSceneDidDismissScreenCallback didDismissCallback;
@property(nonatomic, assign) ROXFloatSceneWillLeaveApplicationCallback willLeaveCallback;
@property(nonatomic, assign) ROXFloatSceneRenderSuccessCallback renderSuccessCallback;
@property(nonatomic, assign) ROXFloatSceneRenderFailedCallback renderFailedCallback;

- (void)setUnityPosition:(ROXUnityAdPosition)position;
- (void)setUnityPositionWithX:(int)x andY:(int)y;
- (void)setUnityPositionRelative:(ROXUnityAdPosition)position offsetX:(int)x offsetY:(int)y;

- (void)setUnityWidth:(int)width height:(int)height;

- (void)showUnity;
- (void)hideUnity;

- (void)removeUnity;

// iOS
- (void)load;

- (BOOL)sceneRenderReady;

@end

NS_ASSUME_NONNULL_END
