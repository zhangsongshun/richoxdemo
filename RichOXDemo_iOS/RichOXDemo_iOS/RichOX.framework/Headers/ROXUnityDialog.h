//
//  ROXUnityDialog.h
//  RichOX
//
//  Created by RichOX on 2020/7/17.
//  Copyright © 2020 richox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ROXTypes.h"
#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface ROXUnityDialog : NSObject

- (instancetype)initWithDialogSceneClient:(ROXTypeDialogSceneClientRef _Nonnull* _Nonnull)client 
                             sceneEntryId:(NSString *)sceneEntryId;

// 引用 Unity 的 DialogSceneClient
@property(nonatomic, assign) ROXTypeDialogSceneClientRef _Nullable* _Nullable dialogSceneClient;
// 广告加载后，回调到 Unity 的接口
@property(nonatomic, assign) ROXDialogSceneDidReceiveCallback receivedCallback;
@property(nonatomic, assign) ROXDialogSceneDidFailToReceiveWithErrorCallback failedCallback;
@property(nonatomic, assign) ROXDialogSceneWillPresentScreenCallback willPresentCallback;
@property(nonatomic, assign) ROXDialogSceneDidDismissScreenCallback didDismissCallback;
@property(nonatomic, assign) ROXDialogSceneWillLeaveApplicationCallback willLeaveCallback;
@property(nonatomic, assign) ROXDialogSceneRenderSuccessCallback renderSuccessCallback;
@property(nonatomic, assign) ROXDialogSceneRenderFailedCallback renderFailedCallback;

- (void)load;
- (BOOL)sceneRenderReady;
- (void)showFromViewController:(UIViewController *)viewController;

@end
NS_ASSUME_NONNULL_END
