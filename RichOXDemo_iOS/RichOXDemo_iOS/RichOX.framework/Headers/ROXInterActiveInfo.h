//
//  ROXInterActiveInfo.h
//  RichOX
//
//  Created by zena.tang on 2020/9/8.
//  Copyright © 2020 richox. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ROXInterActiveInfo : NSObject

@property (nonatomic) BOOL hasTriggered;
@property (nonatomic) NSInteger rewardedNumber;
@property (nonatomic) NSInteger maxNumber;
@property (nonatomic) NSInteger currentNumber;
@property (nonatomic, strong) NSString *extra;

/**
 * 当前活动是否已经第一次展示
 * 可配合 APP 本地策略一起使用
 * @return 展示状态
 */
- (BOOL)hasTriggered;

///**
// * 当前任务获取的奖励信息
// * @return 奖励数量
// */
//- (NSInteger)getRewardedNumber;
//
///**
// * 当前任务可获取的最大奖励数量
// * @return 最大奖励数量
// */
//- (NSInteger)getMaxNumber;
//
///**
// * 当前任务已经获取的奖励数量
// * @return 当前奖励数量
// */
//- (NSInteger)getCurrentNumber;
//
///**
// * 获取的额外信息
// * 供扩展使用
// * @return 额外信息
// */
//- (NSString *)getExtra;

@end

NS_ASSUME_NONNULL_END
