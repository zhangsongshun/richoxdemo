//
//  CommonViewController.h
//  RichOXDemo_iOS
//
//  Created by richox on 2020/7/29.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonViewController : UIViewController

@property (strong, nonatomic) UIViewController *tabVC;

@end

NS_ASSUME_NONNULL_END
