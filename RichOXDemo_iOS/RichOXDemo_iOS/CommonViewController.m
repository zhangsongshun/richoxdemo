//
//  CommonViewController.m
//  RichOXDemo_iOS
//
//  Created by richox on 2020/7/29.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import "CommonViewController.h"
#import <Masonry/Masonry.h>
#import "DataManager.h"
#import "UIView+Toast.h"
#import "AppDelegate.h"
#import "UILabelCopy.h"
#import "macro.h"
@import TaurusXAds;
@import RichOX;
#import "TestTabPageViewController.h"
#import "WXLoginViewController.h"
#import "AppTaskViewController.h"

#import "ViewController.h"
#import "TestTabPageViewController.h"
#import "TestDialogViewController.h"
#import "TestBannerViewController.h"
#import "TestOtherViewController.h"
@import RichOXBase;

@interface CommonViewController ()

@property (nonatomic, strong) UITextField *appIdText;

@property (nonatomic, strong) UITextField *tabText;

@property (nonatomic, strong) UILabelCopy *userIdLab;
@property (nonatomic, strong) UIButton *uuidCoverBtn;
@property (nonatomic, strong) UIButton *bindWeChatBtn;

@end

@implementation CommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *appId = [[UILabel alloc] init];
    [self.view addSubview:appId];
    appId.text = @"Input AppId:";
    //appId.backgroundColor = [UIColor lightGrayColor];
    appId.textColor = [UIColor blueColor];
    appId.textAlignment = NSTextAlignmentCenter;
    
    [appId mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(kTopBarSafeHeight);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@(150));
        make.height.equalTo(@(20));
    }];
    
    
    UITextField *appIdLab = [[UITextField alloc]init];
    appIdLab.text = [DataManager getManager].appId;
    [self.view addSubview:appIdLab];
    appIdLab.borderStyle = UITextBorderStyleRoundedRect;
    
    [appIdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(appId.mas_bottom).offset(10);
        make.right.equalTo(self.view).offset(-20);
        make.left.equalTo(self.view).offset(20);
        make.height.equalTo(@(40));
    }];
    self.appIdText = appIdLab;

    UILabel *testModeLab = [[UILabel alloc] init];
    [self.view addSubview:testModeLab];
    testModeLab.text = @"Test Mode:";
    //fissionKey.backgroundColor = [UIColor lightGrayColor];
    testModeLab.textColor = [UIColor blueColor];
    testModeLab.textAlignment = NSTextAlignmentCenter;
    
    [testModeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(appIdLab.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(20);
        make.width.equalTo(@(150));
        make.height.equalTo(@(20));
    }];
    
    UISwitch *testMode = [[UISwitch alloc] init];
    [self.view addSubview:testMode];
    testMode.on = NO;
    
    [testMode setOnTintColor:[UIColor redColor]];//开启时颜色
//    testMode.tintColor = [UIColor lightGrayColor];
//    [testMode setThumbTintColor:[UIColor whiteColor]];//按钮颜色
    
    [testMode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(testModeLab);
        make.right.equalTo(self.view).offset(-20);
        make.width.equalTo(@(80));
        make.height.equalTo(@(20));
    }];
    
    [testMode addTarget:self action:@selector(setTestMode:) forControlEvents:(UIControlEventValueChanged)];
    
    UILabel *userLab = [[UILabel alloc] init];
    [self.view addSubview:userLab];
    userLab.text = @"USER ID:";
    userLab.textColor = [UIColor blueColor];
    userLab.textAlignment = NSTextAlignmentCenter;

    [userLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(testModeLab.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(20);
        make.height.equalTo(@(40));
        make.width.equalTo(@(80));
    }];
    
    self.userIdLab = [[UILabelCopy alloc] init];
    [self.view addSubview:_userIdLab];
    _userIdLab.backgroundColor = [UIColor lightGrayColor];
    _userIdLab.text = [RichOXBaseManager userId];
    _userIdLab.textColor = [UIColor grayColor];
    _userIdLab.textAlignment = NSTextAlignmentCenter;

    [_userIdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(userLab);
        make.right.equalTo(self.view).offset(-20);
        make.left.equalTo(userLab.mas_right).offset(20);
        make.height.equalTo(@(40));
    }];

    UILabel *tabTitle = [[UILabel alloc] init];
    [self.view addSubview:tabTitle];
    tabTitle.text = @"Input Tab Entry Id:";
    //appId.backgroundColor = [UIColor lightGrayColor];
    tabTitle.textColor = [UIColor blueColor];
    tabTitle.textAlignment = NSTextAlignmentCenter;
    
    [tabTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(userLab.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(20);
        make.width.equalTo(@(180));
        make.height.equalTo(@(30));
    }];
    
    UITextField *tabLab = [[UITextField alloc]init];
    [self.view addSubview:tabLab];
    tabLab.borderStyle = UITextBorderStyleRoundedRect;
    
    [tabLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tabTitle);
        make.right.equalTo(self.view).offset(-20);
        make.left.equalTo(tabTitle.mas_right).offset(20);
        make.height.equalTo(@(30));
    }];
    self.tabText = tabLab;
    
    UIButton *initSDKBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:initSDKBtn];
    initSDKBtn.layer.cornerRadius = 5;
    initSDKBtn.backgroundColor  = [UIColor colorWithRed:47.0/255.0 green:147.0/255.0 blue:203.0/255.0 alpha:1.0];
    [initSDKBtn setTitle:@"Start" forState:UIControlStateNormal];
    [initSDKBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:28]];
    [initSDKBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [initSDKBtn addTarget:self action:@selector(startSDK) forControlEvents:UIControlEventTouchUpInside];
    
    [initSDKBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tabLab.mas_bottom).offset(20);
        //make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-kBottomSafeHeight-20);
    }];
    
    
    UIButton *bindWeChatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:bindWeChatBtn];
    bindWeChatBtn.layer.cornerRadius = 5;
    bindWeChatBtn.backgroundColor  = [UIColor colorWithRed:0.04 green:0.73 blue:0.03 alpha:1.00];
    [bindWeChatBtn setTitle:@"Binding WeChat" forState:UIControlStateNormal];
    bindWeChatBtn.titleLabel.numberOfLines = 0;
    [bindWeChatBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [bindWeChatBtn setTitle:@"Binded WeChat" forState:UIControlStateDisabled];
    [bindWeChatBtn addTarget:self action:@selector(bindingWeChat) forControlEvents:UIControlEventTouchUpInside];
    self.bindWeChatBtn = bindWeChatBtn;
    
    [bindWeChatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(initSDKBtn);
        make.right.equalTo(self.view).offset(-10);
        make.left.equalTo(initSDKBtn.mas_right).offset(15);
        make.width.equalTo(@(90));
        make.height.equalTo(@(50));
    }];
    

    UIButton *getUidBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:getUidBtn];
    getUidBtn.layer.cornerRadius = 5;
    getUidBtn.backgroundColor  = [UIColor colorWithRed:203.0/255.0 green:203.0/255.0 blue:203.0/255.0 alpha:1.0];
    [getUidBtn setTitle:@"Look UUID" forState:UIControlStateNormal];
    getUidBtn.titleLabel.numberOfLines = 0;
    [getUidBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [getUidBtn addTarget:self action:@selector(getUUID) forControlEvents:UIControlEventTouchUpInside];
    
    [getUidBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(initSDKBtn);
        make.right.equalTo(self.view).offset(-10);
        make.left.equalTo(initSDKBtn.mas_right).offset(15);
        make.width.equalTo(@(90));
        make.height.equalTo(@(50));
    }];
    
    UIButton *appTaskBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:appTaskBtn];
    appTaskBtn.layer.cornerRadius = 5;
    appTaskBtn.titleLabel.numberOfLines = 0;
    appTaskBtn.backgroundColor  = [UIColor colorWithRed:217.0/255.0 green:47.0/255.0 blue:73.0/255.0 alpha:1.0];
    [appTaskBtn setTitle:@"APP Task" forState:UIControlStateNormal];
    [appTaskBtn setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
    [appTaskBtn addTarget:self action:@selector(doAppTask) forControlEvents:UIControlEventTouchUpInside];
    
    [appTaskBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(initSDKBtn);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(initSDKBtn.mas_left).offset(-15);
        make.width.equalTo(@(60));
        make.height.equalTo(@(50));
    }];
    
    
    UIButton *clearCacheBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:clearCacheBtn];
    [clearCacheBtn setBackgroundColor:[UIColor colorWithRed:248.0/255.0 green:171.0/255.0 blue:39.0/255.0 alpha:1.0]];
    [clearCacheBtn setTitle:@"Clear Cache" forState:UIControlStateNormal];
    clearCacheBtn.titleLabel.numberOfLines = 0;
    clearCacheBtn.layer.cornerRadius = 5;
    [clearCacheBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [clearCacheBtn addTarget:self action:@selector(clearCache) forControlEvents:UIControlEventTouchUpInside];
    
    [clearCacheBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(initSDKBtn);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(initSDKBtn.mas_left).offset(-15);
        make.width.equalTo(@(60));
        make.height.equalTo(@(50));
    }];
    
}

- (void)setTestMode:(UISwitch *)s {
    if (s.on) {
        [RichOXBaseManager setTestMode:YES];
    } else {
        [RichOXBaseManager setTestMode:NO];
    }
}

- (void)saveData {
    if (![DataManager saveDataToLocal]) {
        dispatch_async(dispatch_get_main_queue(), ^{
        [self.view makeToast:@"save data failed" duration:3.0 position:CSToastPositionCenter];
        });
    }
}

- (void)clearData {
    self.userIdLab.text = @"";
    self.appIdText.text = @"";
    
    [DataManager clearLocalData];
}

- (void)startSDK {
    //[ROXDemoHelper getUserId:deviceId success: ^(NSString *userId) {
    if ([DataManager getManager].appId != nil && ![[DataManager getManager].appId isEqualToString:@""]) {
        [self saveData];
        //[ROXManager initWithAppId:[DataManager getManager].appId userId: nil];
        [RichOXBaseManager initWithAppId:[DataManager getManager].appId initSuccess:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                TestTabPageViewController *tabvc = (TestTabPageViewController *)(self.tabVC);
                if (self.tabText.text != nil && ![self.tabText.text isEqualToString:@""]) {
                    [tabvc loadTabView:self.tabText.text];
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [tabvc setContentStr:@"please input tab scene entrance id then click \"start\" button on common tab to load the scene"];
                    });
                }
            });
        }];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
        [self.view makeToast:@"param error" duration:3.0 position:CSToastPositionCenter];
        });
    }
    
    [self presentTestPage];
}

- (void)getUUID {
    if (self.uuidCoverBtn == nil) {
        self.uuidCoverBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.uuidCoverBtn.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        self.uuidCoverBtn.backgroundColor  = [UIColor clearColor];
        [self.uuidCoverBtn addTarget:self action:@selector(closeUUID) forControlEvents:UIControlEventTouchUpInside];
        
        UILabelCopy *cover = [[UILabelCopy alloc] init];
        cover.frame = CGRectMake(0, 0, 300, 80);
        cover.center = CGPointMake(self.uuidCoverBtn.frame.size.width * 0.5, self.uuidCoverBtn.frame.size.height * 0.5);
        cover.font = [UIFont systemFontOfSize:18];
        cover.text = [TXAD getUid];
        cover.textColor = [UIColor whiteColor];
        cover.numberOfLines = 0;
        cover.textAlignment = NSTextAlignmentCenter;
        cover.backgroundColor = [UIColor blackColor];
        cover.alpha = 0.8;
        cover.clipsToBounds = YES;
        cover.layer.cornerRadius = 5;
        [self.uuidCoverBtn addSubview:cover];
    }
    
    [self.view addSubview:self.uuidCoverBtn];
}

- (void)closeUUID {
    [self.uuidCoverBtn removeFromSuperview];
}

- (void)clearCache {
    NSFileManager * manager = [NSFileManager defaultManager];
    
    NSString * cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    cachePath = [cachePath stringByAppendingPathComponent:@"richox"];
    NSString *cacheFolder = [cachePath stringByAppendingPathComponent:@"userinfo"];
    
    NSArray *pathsArr = [manager subpathsAtPath:cacheFolder];/*取得文件列表*/
    for (int i = 0; i< pathsArr.count; i++) {
        NSString *subPath = pathsArr[i];
        NSString *filePath = [cacheFolder stringByAppendingPathComponent:subPath];
        // 忽略隐藏文件
        if([filePath containsString:@".DS"]) continue;
        // 判断是否是文件夹
        BOOL isDirectory;
        BOOL isExist =  [manager fileExistsAtPath:filePath isDirectory:&isDirectory];
        if(isDirectory || !isExist) continue;

        [manager removeItemAtPath:filePath error:NULL];
    }
    
    return;
}

- (void)bindingWeChat {
    WXLoginViewController *wxvc = [[WXLoginViewController alloc] init];
    [self presentViewController:wxvc animated:YES completion:nil];
}

- (void)doAppTask {
    AppTaskViewController *wxvc = [[AppTaskViewController alloc] init];
    [self presentViewController:wxvc animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)backToCommon {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)presentTestPage {
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.title = @"互动激励场景";
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:tabBarController];
    nc.modalPresentationStyle = UIModalPresentationFullScreen;
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithTitle:@"back" style:UIBarButtonItemStylePlain target:self action:@selector(backToCommon)];
    tabBarController.navigationItem.leftBarButtonItem = backItem;
    
    //[nc.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
    [nc.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor brownColor],NSFontAttributeName:[UIFont systemFontOfSize:16.0f]}];

    ViewController *vc1 = [[ViewController alloc] init];
    //vc1.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"tab1" image:unselecteImage selectedImage:selectedImage];∂
    vc1.title = @"float";
    
    TestBannerViewController *vc2 = [[TestBannerViewController alloc] init];
    //vc1.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"tab1" image:unselecteImage selectedImage:selectedImage];
    vc2.title = @"banner";

    
    TestTabPageViewController *vc3 = [[TestTabPageViewController alloc] init];
    vc3.title = @"tab";
    self.tabVC = vc3;
    
    TestDialogViewController *vc4 = [[TestDialogViewController alloc] init];
    vc4.title = @"dialog";
    
    
    TestOtherViewController *vc5 = [[TestOtherViewController alloc] init];
    vc5.title = @"other";
    
    NSArray *viewControllers = [NSArray arrayWithObjects:vc1, vc2, vc3, vc4, vc5, nil];
    tabBarController.viewControllers = viewControllers;
    
    [self presentViewController:nc animated:YES completion:nil];
}

@end
