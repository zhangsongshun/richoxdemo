//
//  ChatMessageTableViewCell.h
//  RichOXDemo
//
//  Created by richox on 2021/7/27.
//  Copyright © 2021 RichOX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RichOXToolBox/RichOXChatMessage.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatMessageTableViewCell : UITableViewCell

- (void)setMessageInfo:(RichOXChatMessage *)message;

@end

NS_ASSUME_NONNULL_END
