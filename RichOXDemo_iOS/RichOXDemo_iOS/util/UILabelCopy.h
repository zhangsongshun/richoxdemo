//
//  UILabelCopy.h
//  RichOXDemo_iOS
//
//  Created by richox on 2020/7/30.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabelCopy : UILabel

@end

NS_ASSUME_NONNULL_END
