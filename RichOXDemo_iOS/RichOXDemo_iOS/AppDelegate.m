//
//  AppDelegate.m
//  RichOXDemo_iOS
//
//  Created by richox on 2020/7/29.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import "AppDelegate.h"
@import RichOX;

#import "CommonViewController.h"

#import <GoogleSignIn/GoogleSignIn.h>
#import "WXApi.h"
#import "WXLoginViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@import RichOXBase;
@import RichOXFissionSdk;

#define TEST_APP_HOST @"https://api-test.freeqingnovel.com"
#define TEST_APP_KEY @"xjdN82dpqMf"


@interface AppDelegate ()<WXApiDelegate>

@end

@implementation AppDelegate

//@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //[ROXManager setTestMode:YES];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Info"ofType:@"plist"];
                
    NSDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    if (dictionary != nil && dictionary[@"socialAccount"]) {
        NSString *universalLink = dictionary[@"socialAccount"][@"universalLink"];
        NSDictionary *wxDic = dictionary[@"socialAccount"][@"weixin"];
        if (wxDic != nil) {
            NSString *wxAppID = wxDic[@"appID"];
            [WXApi registerApp:wxAppID universalLink:universalLink];
        }

    }
    
    [ROXManager setBindWeChatBlock:^(ROXBindWeChatCallBack  _Nonnull block) {
        WXLoginViewController *wxvc = [[WXLoginViewController alloc] init];
        
        wxvc.bindingCallBack = block;
        
        [[self getCurrentVC] presentViewController:wxvc animated:YES completion:nil];
    }];
    
    
    
    [RichOXBaseManager setLogEnable:YES];
    
    [RichOXBaseManager setOverSea];
    
    //[RichOXBaseManager setTestMode:YES];
    [RichOXBaseManager setDeviceId:@"ios_test_zly_one"];
    
    [RichOXBaseManager setEventBlock:^(NSString * _Nonnull eventName, NSDictionary * _Nullable param) {
        NSLog(@"OnEventTracker: %@, %@",eventName, param);
    }];
    
    [GIDSignIn sharedInstance].clientID = @"";
        
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    CommonViewController *vc = [[CommonViewController alloc] init];
    //vc1.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"tab1"
    
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];

    
    return YES;
}


- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([url.absoluteString hasPrefix:@"wx"]){
        return [WXApi handleOpenURL:url delegate:self];
    } else if ([url.absoluteString hasPrefix:@"fb"]){
        [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url options:options];
    } else if ([url.host isEqualToString:@"safepay"]) {
            // 支付跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
            }];
            
            // 授权跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
                // 解析 auth code
                NSString *result = resultDic[@"result"];
                NSString *authCode = nil;
                if (result.length>0) {
                    NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                    for (NSString *subResult in resultArr) {
                        if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                            authCode = [subResult substringFromIndex:10];
                            break;
                        }
                    }
                }
                NSLog(@"授权结果 authCode = %@", authCode?:@"");
            }];
    } else {
        [RichOXFission fetchDynamicLinkFromFirebase:url];
    }
    
    return YES;
}

#pragma mark - WXApiDelegate
- (void)onResp:(BaseResp *)resp{
    if([resp isKindOfClass:[SendAuthResp class]]){
        SendAuthResp *resp2 = (SendAuthResp *)resp;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"wxLogin" object:resp2];
    }else{
        NSLog(@"授权失败");
    }
}

- (UIViewController *)getCurrentVC {
    UIViewController *result = nil;
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows) {
            if (tmpWin.windowLevel == UIWindowLevelNormal) {
                window = tmpWin;
                break;
            }
        }
    }
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]]) {
        result = nextResponder;
    } else {
        result = window.rootViewController;
    }
    return result;
}

@end

