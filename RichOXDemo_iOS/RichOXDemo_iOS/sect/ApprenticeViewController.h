//
//  ApprenticeViewController.h
//  RichOXDemo
//
//  Created by richox on 2021/1/18.
//  Copyright © 2021 richox. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ApprenticeViewController : UIViewController

@property(nonatomic) BOOL overSea;

@end

NS_ASSUME_NONNULL_END
