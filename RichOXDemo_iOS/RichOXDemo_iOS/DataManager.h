//
//  DataManager.h
//  RichOXDemo_iOS
//
//  Created by richox on 2020/7/29.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataManager : NSObject

+ (instancetype)getManager;

@property (nonatomic, strong, nullable) NSString *appId;

+ (BOOL)saveDataToLocal;

+ (void)clearLocalData;

@end

NS_ASSUME_NONNULL_END
