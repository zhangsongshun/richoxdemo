package com.richox.demo;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.bytedance.sdk.openadsdk.TTAdConfig;
import com.bytedance.sdk.openadsdk.TTAdSdk;
import com.richox.base.CommonBuilder;
import com.richox.base.EventCallback;
import com.richox.base.InitCallback;
import com.richox.base.RichOX;
import com.richox.demo.constance.Constants;
import com.richox.toolbox.RichOXToolbox;
import com.we.modoo.ModooHelper;

import org.json.JSONObject;

import java.util.Map;
import java.util.Set;

import we.studio.embed.EmbedSDK;
import we.studio.embed.metasec.MetaSecHelper;

public class RichOXApplication extends Application {
    private String TAG = "RichOXApplication";

    @Override
    public void onCreate() {
        super.onCreate();
        initWeChat();
        initRichOX();
        EmbedSDK.getInstance().init(getApplicationContext());
        TTAdConfig.Builder builder = new TTAdConfig.Builder()
                .appId("223469");
        TTAdSdk.init(this, builder.build());
        String license = "8iirlWnqEE2d1Tz0Rk2tRql+obhjHLo+A4HzTyoBw0KvI05KD8ri0z8sRjqdE7sBvJAvzyPZFWAWMo7Bh+ciKH33MhOKYM4PLco7mR1a+P0rBhDr8PyJrTEDye0M0mKhA79Wnu9XrbO364VgIMDlcThSB2VW8ZxwXgmt9dWZFYo/jsgnEwndwN3liaevoQtnnnNO/61XxEYKgT6myILxetk1vERqQ64Vv4twfE8mi5j8TP3IPbhMFdNfs8S3Ng7clmM2IyAMqxdYr+7Phar/ZTvarpRYAQjt+L3Wd+sT9j1GV51D95kJ3WztALikGbPeszv2ZaP3wyEtZ3AUpQo6wjecvH5OROSiOuzDEDDKBi67CTZMP06tM631/EK7D1OmhAzt9++oY0K0AbEhS/Qg+Ww/NONj5Aq2jA8uWn0k6Ki3yltz5V8Cu4j67i8X4Lg2rP2EO7I8RTudJrEZWHnVnNk/e4tqIpVWN+Qn9afNgw/uNwCVque1x+Rp+JmiVG8IgsZGTcMhIiBT4zRMqc86Rs6cET6hCXZGhkpJGIqhJuSAIer2zKoIcOghcZDpzb6xjis38A==";
        MetaSecHelper.getInstance().init(this, "223469", license,
                new MetaSecHelper.OnDeviceIDListener() {
                    @Override
                    public void onIdLoaded(String deviceId, String installId) {
                        // 此回调在穿山甲 SDK 初始化成功之后才有（通过 TaurusX 加载穿山甲广告）
                        // deviceId 即为火山引擎 id
                        Log.e(TAG, "onIdLoaded, deviceId: " + deviceId);
                    }
                });
        RichOXToolbox.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    private void initRichOX() {
        RichOX.setTestMode(true);
        String deviceId = RichOX.genDefaultDeviceId(getApplicationContext());
        Log.d(TAG, "the deviceid " + deviceId);
        CommonBuilder builder = new CommonBuilder.Builder()
                .setAppId(Constants.APP_ID)
//                .setAppKey("nnDlq82zDl")
//                .setHostUrl("https://api-d418.freeqingnovel.com")
//                .setDeviceId("60fe37e31ca34a17_eee595b23659d099995c682fd6b879d2") // S2AEZL
//                .setDeviceId("fission_tong_test_1")
//                .setDeviceId("fission_test_invitation")
//                .setDeviceId("fission_test_invitation_2")
//                .setDeviceId("10100386-10100386")
//                .setDeviceId("richox2_tester")
//                .setDeviceId("richox_tester")
                .setDeviceId("hema_tong_test")
//                .setDeviceId("test_tong_01")
//                .setDeviceId("share_h5_test6")
//                .setDeviceId("zly_user_four_2")
//                .setDeviceId("test_zly_user_1")
//                .setPlatformId("S150")
//                .setChannel("test")
                .build();
        RichOX.init(getApplicationContext(), builder, new InitCallback() {
            @Override
            public void onSuccess() {
                Log.d(Constants.TAG, "Init success");
                Log.d(Constants.TAG, "the result is " + RichOX.hasInitiated());
            }

            @Override
            public void onFailed(int code, String msg) {
                Log.d(Constants.TAG, "Init result code is : " + code + " msg is : " + msg);
            }
        });

        RichOX.registerEventCallback(new EventCallback() {
            @Override
            public void onEvent(String name) {
                Log.d("event", "the name is " + name);
                // 自行打点
                // AppLog.onEventV3(name);
            }

            @Override
            public void onEvent(String name, String value) {
                Log.d("event", "the name is " + name + " and the value is " + value);
                try {
                    JSONObject object = new JSONObject();
                    object.putOpt(name, value);
                    // 自行打点
                    // AppLog.onEventV3(name, object);
                } catch (Exception e) {

                }

            }

            @Override
            public void onEvent(String name, Map<String, Object> map) {
                Log.d("event", "the name is " + name + " and the map is " + map.toString());
                try {
                    JSONObject object = new JSONObject();
                    Set<String> keys = map.keySet();
                    for (String key : keys) {
                        object.putOpt(key, map.get(key));
                    }
                    // 自行打点
                    // AppLog.onEventV3(name, object);
                } catch (Exception e) {

                }
            }

            @Override
            public void onEventJson(String name, String jsonString) {
                try {
                    // 自行打点
                    // AppLog.onEventV3(name, new JSONObject(jsonString));
                } catch (Exception e) {

                }
            }
        });
        RichOX.genDefaultDeviceId(this);
        ModooHelper.init(this);
    }

    private void initWeChat() {
        ModooHelper.init(this);
    }
}
