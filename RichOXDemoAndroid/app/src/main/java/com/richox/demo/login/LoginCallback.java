package com.richox.demo.login;

public interface LoginCallback {
    void onSuccess(UserInfo user);

    void onFailed(String msg);
}
