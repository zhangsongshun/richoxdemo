package com.richox.demo.util;

import android.app.Activity;
import android.widget.Toast;

public class ToastUtil {
    public static void showToast(Activity activity, String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
