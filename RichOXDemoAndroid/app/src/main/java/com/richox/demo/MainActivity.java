package com.richox.demo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.richox.demo.activity.ROXSectActivity;
import com.richox.demo.activity.ROXUserActivity;
import com.richox.demo.activity.RichOXCommonActivity;
import com.richox.demo.activity.RichOXCommonUserActivity;
import com.richox.demo.activity.RichOXH5Activity;
import com.richox.demo.activity.RichOXNormalStrategyActivity;
import com.richox.demo.activity.RichOXNormalStrategyCustomActivity;
import com.richox.demo.activity.RichOXSectActivity;
import com.richox.demo.activity.RichOXShareActivity;
import com.richox.demo.activity.RichOXStageStrategyActivity;
import com.richox.demo.activity.EventActivity;
import com.richox.demo.activity.ToolboxActivity;

public class MainActivity extends Activity {
    private final String TAG = "FissionDemo";

    private TextView mUserActivity;
    private TextView mROXUserActivity;
    private TextView mH5Activity;
    private TextView mStrategyActivity2;
    private TextView mNormalStrategyActivity;
    private TextView mNormalCustomActivity;
    private TextView mShareActivity;
    private TextView mSectActivity;
    private TextView mROXSectActivity;
    private TextView mEventActivity;
    private TextView mToolboxActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_richox_main);
        initView();
    }


    private void initView() {
        mUserActivity = findViewById(R.id.demo_activity_richox_user);
        mUserActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, RichOXCommonUserActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mROXUserActivity = findViewById(R.id.demo_activity_rox_user);
        mROXUserActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, ROXUserActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mH5Activity = findViewById(R.id.demo_activity_richox_h5);
        mH5Activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, RichOXH5Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mStrategyActivity2 = findViewById(R.id.demo_activity_richox_strategy2);
        mStrategyActivity2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, RichOXStageStrategyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mNormalStrategyActivity = findViewById(R.id.demo_activity_richox_normal_strategy);
        mNormalStrategyActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, RichOXNormalStrategyActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mNormalCustomActivity = findViewById(R.id.demo_activity_richox_normal_strategy_custom);
        mNormalCustomActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, RichOXNormalStrategyCustomActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mShareActivity = findViewById(R.id.demo_activity_richox_share);
        mShareActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, RichOXShareActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mSectActivity = findViewById(R.id.demo_activity_richox_sect);
        mSectActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, RichOXSectActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mROXSectActivity = findViewById(R.id.demo_activity_rox_sect);
        mROXSectActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, ROXSectActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mEventActivity = findViewById(R.id.demo_activity_richox_event);
        mEventActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, EventActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        mToolboxActivity = findViewById(R.id.demo_activity_richox_toolbox);
        mToolboxActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, ToolboxActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

}
