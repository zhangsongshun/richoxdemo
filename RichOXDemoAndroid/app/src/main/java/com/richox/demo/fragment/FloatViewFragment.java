package com.richox.demo.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.richox.demo.R;

public class FloatViewFragment extends Fragment {
    private static final String TAG = "FloatFragment";
    private TextView mLottieLoad;
    private FrameLayout mLottieContent;

    private TextView mImageLoad;
    private FrameLayout mImageContent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_richox_h5_floatview, null);
        return view;
    }

    @Override
    public void onResume() {

        super.onResume();
    }

//    private void initView(View view) {
//        mLottieLoad = view.findViewById(R.id.float_lottie);
//        mLottieContent = view.findViewById(R.id.float_lottie_content);
//        FloatScene scene = new FloatScene(getContext(), "50121", mLottieContent);
//        scene.setSceneListener(new SceneListener() {
//            @Override
//            public void onLoaded() {
//                Log.d(TAG, "on loaded");
//                if (scene.isReady()) {
//                    Toast.makeText(getContext(), "加载成功", Toast.LENGTH_SHORT).show();
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mLottieContent.setVisibility(View.VISIBLE);
//                        }
//                    });
//
//                }
//            }
//
//            @Override
//            public void onLoadFailed(RichOXError error) {
//                Log.d(TAG, "on onLoadFailed");
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getContext(), "加载失败：" + error.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//
//            @Override
//            public void onShown() {
//                Log.d(TAG, "on onShown");
//            }
//
//            @Override
//            public void onRenderSuccess() {
//
//            }
//
//            @Override
//            public void onRenderFailed(RichOXError error) {
//
//            }
//
//            @Override
//            public void onClick() {
//                Log.d(TAG, "on onClick");
//            }
//
//            @Override
//            public void onClose() {
//                Log.d(TAG, "on onClose");
//            }
//        });
//        mLottieLoad.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                scene.load();
//            }
//        });
//
//        mImageLoad = view.findViewById(R.id.float_image);
//        mImageContent = view.findViewById(R.id.float_image_content);
//        FloatScene scene2 = new FloatScene(getContext(), "50083", mImageContent);
////        FloatScene scene2 = new FloatScene(getContext(), "40032", mImageContent); // 204 sign
//        scene2.setSceneListener(new SceneListener() {
//            @Override
//            public void onLoaded() {
//                Log.d(TAG, "on loaded");
//                if (scene2.isReady()) {
//                    Toast.makeText(getContext(), "加载成功", Toast.LENGTH_SHORT).show();
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mImageContent.setVisibility(View.VISIBLE);
//                        }
//                    });
//                }
//            }
//
//            @Override
//            public void onLoadFailed(RichOXError error) {
//                Log.d(TAG, "on onLoadFailed" + error.toString());
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getContext(), "加载失败：" + error.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//
//            @Override
//            public void onShown() {
//                Log.d(TAG, "on onShown");
//            }
//
//            @Override
//            public void onRenderSuccess() {
//
//            }
//
//            @Override
//            public void onRenderFailed(RichOXError error) {
//
//            }
//
//            @Override
//            public void onClick() {
//                Log.d(TAG, "on onClick");
//            }
//
//            @Override
//            public void onClose() {
//                Log.d(TAG, "on onClose");
//            }
//        });
////        scene2.load();
//        mImageLoad.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                scene2.load();
//            }
//        });
//
//        view.findViewById(R.id.test_guoyuan).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                boolean isActive = scene2.isInterActive("orchard");
//                if (isActive) {
//                    scene2.setInterActiveListener(new InterActiveListener() {
//                        @Override
//                        public void initialized(boolean status, InterActiveInfo interActiveInfo) {
//                            if (status) {
//                                Log.d(TAG, interActiveInfo.toString());
//                            }
//                        }
//
//                        @Override
//                        public void updateFromServer(int id, boolean status, InterActiveInfo info) {
//                            if (status) {
//                                Log.d(TAG, "the id : " + id + " and info : " + info.toString());
//                            }
//                        }
//
//                        @Override
//                        public void updateStatusFormH5(int id, boolean status, int progress) {
//                            Log.d(TAG, "the id : " + id + " status: " + status + " progress " + progress);
//                        }
//                    });
//                    scene2.loadInterActiveInfo();
//                    scene2.submitInterActiveTask(3);
//                    scene2.fetchInterActiveTaskStatus(4);
//                }
//            }
//        });
//
//        // 签到
//        scene2.setMissionListener(new ActivityMissionListener() {
//            @Override
//            public void update(int taskId, MissionInfo info) {
//                Log.d(TAG, "the taskId: " + taskId + " and the mission info" + info.toString());
//            }
//        });
//        view.findViewById(R.id.test_qiandao).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                scene2.fetchActivityMissionStatus(1, 6);
//            }
//        });
//    }

}
