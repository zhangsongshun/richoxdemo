package com.richox.demo;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.richox.base.CommonBuilder;
import com.richox.base.EventCallback;
import com.richox.base.InitCallback;
import com.richox.base.RichOX;
import com.richox.demo.constance.Constants;
import com.we.modoo.ModooHelper;

import org.json.JSONObject;

import java.util.Map;
import java.util.Set;

public class RichOXApplication extends Application {
    private String TAG = "RichOXApplication";

    @Override
    public void onCreate() {
        super.onCreate();
        initWeChat();
        initRichOX();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    private void initRichOX() {
        RichOX.setTestMode(true);
        String deviceId = RichOX.genDefaultDeviceId(getApplicationContext());
        Log.d(TAG, "the deviceid " + deviceId);
        CommonBuilder builder = new CommonBuilder.Builder()
                .setAppId(Constants.APP_ID)
//                .setAppKey("nnDlq82zDl")
//                .setHostUrl("https://api-d418.freeqingnovel.com")
//                .setDeviceId("60fe37e31ca34a17_eee595b23659d099995c682fd6b879d2") // S2AEZL
//                .setDeviceId("fission_tong_test_1")
//                .setDeviceId("fission_test_invitation")
//                .setDeviceId("fission_test_invitation_2")
//                .setDeviceId("10100386-10100386")
//                .setDeviceId("richox2_tester")
                .setDeviceId("test_tong_06_11")
//                .setDeviceId("test_tong22")
//                .setDeviceId("test_tong23")
//                .setDeviceId("test_tong_01")
//                .setDeviceId("share_h5_test6")
//                .setDeviceId("zly_user_four_2")
//                .setDeviceId("test_zly_user_1")
                .setPlatformId("S150")
                .setChannel("test")
                .build();
        RichOX.init(getApplicationContext(), builder, new InitCallback() {
            @Override
            public void onSuccess() {
                Log.d(Constants.TAG, "Init success");
            }

            @Override
            public void onFailed(int code, String msg) {
                Log.d(Constants.TAG, "Init result code is : " + code + " msg is : " + msg);
            }
        });

        Log.d(Constants.TAG, "the appid is  : " +RichOX.getAppId());

        RichOX.registerEventCallback(new EventCallback() {
            @Override
            public void onEvent(String name) {
                Log.d("event", "the name is " + name);
                // 自行打点
                // AppLog.onEventV3(name);
            }

            @Override
            public void onEvent(String name, String value) {
                Log.d("event", "the name is " + name + " and the value is " + value);
                try {
                    JSONObject object = new JSONObject();
                    object.putOpt(name, value);
                    // 自行打点
                    // AppLog.onEventV3(name, object);
                } catch (Exception e) {

                }

            }

            @Override
            public void onEvent(String name, Map<String, Object> map) {
                Log.d("event", "the name is " + name + " and the map is " + map.toString());
                try {
                    JSONObject object = new JSONObject();
                    Set<String> keys = map.keySet();
                    for (String key : keys) {
                        object.putOpt(key, map.get(key));
                    }
                    // 自行打点
                    // AppLog.onEventV3(name, object);
                } catch (Exception e) {

                }
            }

            @Override
            public void onEventJson(String name, String jsonString) {
                try {
                    // 自行打点
                    // AppLog.onEventV3(name, new JSONObject(jsonString));
                } catch (Exception e) {

                }
            }
        });
        RichOX.genDefaultDeviceId(this);
        ModooHelper.init(this);
    }

    private void initWeChat() {
        ModooHelper.init(this);
    }
}
